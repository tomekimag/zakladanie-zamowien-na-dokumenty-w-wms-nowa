if exists (select 1 from sysobjects where name = 'IMAG_twit_dodaj_zamowienia_od_odbiorcy_z_WMS' and type = 'P')
   drop procedure dbo.IMAG_twit_dodaj_zamowienia_od_odbiorcy_z_WMS
go
create procedure dbo.IMAG_twit_dodaj_zamowienia_od_odbiorcy_z_WMS
	@IdFirmy numeric, @IdMagazynu numeric, @IdUzytkownika numeric, @REFNO bigint, 
	@NumerZamowienia varchar(30) output
as
begin 

	set xact_abort on 
	set transaction isolation level REPEATABLE READ 

	begin transaction

	set @NumerZamowienia = ''
		
	declare @id_zamowienia numeric, @id_pozycji_zamowienia numeric

	declare @burttoNetto char(6), @IdKontrahenta numeric

	set @burttoNetto = 'Netto'

	declare @TYPDOK varchar(3), @NRDOKUMENTU varchar(20), @DDOWOD date, @DATFAKTURY date, @NRIDODN bigint, @UWAGI varchar(max), @NRAWIZO varchar(20), @DOKUMWZ varchar(20)

	select top 1 @TYPDOK= TYPDOK, @NRDOKUMENTU = NRDOKUMENTU, @DDOWOD = DDOWOD, @DATFAKTURY = DATFAKTURY, @NRIDODN = NRIDODN, @UWAGI = UWAGI, @NRAWIZO = NRAWIZO, @DOKUMWZ = DOKUMWZ 
	from dbo.v_dpmag_imag v where REFNO = @REFNO
	
	set @IdKontrahenta = @NRIDODN

	declare @Data Int
	set @Data = cast(cast(@DATFAKTURY as datetime) as int) + 36163

	exec RM_DodajZamowienie_Server @IdFirmy, @IdKontrahenta, @IdMagazynu, 1, @Data, @IdUzytkownika, 0, @burttoNetto, 1, @id_zamowienia output


	declare @ID_ARTYKULU numeric
	declare @KOD_VAT varchar(3)
	declare @ZAMOWIONO decimal(16, 6)
	declare @ZREALIZOWANO decimal(16, 6)
	declare @ZAREZERWOWANO decimal(16, 6)
	declare @DO_REZERWACJI decimal(16, 6)
	declare @CENA_NETTO decimal(14, 4)
	declare @CENA_BRUTTO decimal(14, 4)
	declare @CENA_NETTO_WAL decimal(14, 4)
	declare @CENA_BRUTTO_WAL decimal(14, 4)
	declare @PRZELICZNIK decimal(16, 6)
	declare @JEDNOSTKA varchar(10)
	declare @NARZUT decimal(8, 4)
	declare @OPIS varchar(500)
	declare @znak_narzutu tinyint
	declare @TRYB_REJESTRACJI tinyint
	declare @ID_DOSTAWY_REZ numeric
	declare @ID_WARIANTU_PRODUKTU numeric
	declare @ZNACZNIK_CENY char(1)
	declare @ID_JEDNOSTKI numeric, @VAT_ZAKUPU char(3), @JEDNOSTKA_SKROT varchar(10)

	declare @RetStat int

	declare @NRIDASN bigint, @ILOSC decimal(15,3), @UWAGIPOZ varchar(max), @NRIDWMS bigint

	declare kursor_po_poz cursor local fast_forward for
	select v.NRIDASN, a.ID_JEDNOSTKI, v.ILOSC, v.UWAGIPOZ, v.NRIDWMS, a.VAT_ZAKUPU
	from dbo.v_dpmag_imag v 
	join ARTYKUL a on v.NRIDASN = a.ID_ARTYKULU
	where REFNO = @REFNO
		
	OPEN kursor_po_poz
	
	FETCH NEXT FROM kursor_po_poz into @NRIDASN, @ID_JEDNOSTKI, @ILOSC, @UWAGIPOZ, @NRIDWMS, @VAT_ZAKUPU
	
	WHILE @@FETCH_STATUS = 0
	BEGIN

		set @id_artykulu = @NRIDASN
		set @OPIS = @UWAGIPOZ
		set @KOD_VAT = @VAT_ZAKUPU

		select top 1 @JEDNOSTKA_SKROT = SKROT from JEDNOSTKA where ID_JEDNOSTKI = @ID_JEDNOSTKI

		set @ZAMOWIONO = case when @ILOSC < 0 then @ILOSC * (-1) else @ILOSC end
		set @ZREALIZOWANO = 0
		set @NARZUT = 0
		set @znak_narzutu = 2
		set @TRYB_REJESTRACJI = 0
		set @ID_DOSTAWY_REZ = 0
		set @ID_WARIANTU_PRODUKTU = 0
		set @ZNACZNIK_CENY = 0
		set @DO_REZERWACJI = @ZAMOWIONO
		set @ZAREZERWOWANO = 0
		set @CENA_NETTO = 0
		set @CENA_BRUTTO = 0
		set @CENA_NETTO_WAL = 0
		set @CENA_BRUTTO_WAL = 0
		set @PRZELICZNIK = 1

		exec @RetStat = RM_DodajPozycjeZamowienia_Server @id_pozycji_zamowienia output, @id_zamowienia, @ID_ARTYKULU, @KOD_VAT, @ZAMOWIONO,
			@ZREALIZOWANO, @ZAREZERWOWANO, @DO_REZERWACJI, @CENA_NETTO, @CENA_BRUTTO, @CENA_NETTO_WAL,
			@CENA_BRUTTO_WAL, @PRZELICZNIK, @JEDNOSTKA_SKROT, @NARZUT, @OPIS, @znak_narzutu, @TRYB_REJESTRACJI, 
			@ID_DOSTAWY_REZ, @ID_WARIANTU_PRODUKTU, @ZNACZNIK_CENY

		if @RetStat = 0 goto Error

		/*
		update POZYCJA_ZAMOWIENIA set
			POLE5 = @PART--,
	--		POLE1 = @pole1_karton
		where ID_POZYCJI_ZAMOWIENIA = @id_pozycji_zamowienia
		*/

		FETCH NEXT FROM kursor_po_poz into @NRIDASN, @ID_JEDNOSTKI, @ILOSC, @UWAGIPOZ, @NRIDWMS, @VAT_ZAKUPU
	END

	CLOSE kursor_po_poz
	DEALLOCATE kursor_po_poz


	declare @suma_netto decimal(16, 4) 
	declare @suma_brutto decimal(16, 4)  
	declare @suma_netto_wal decimal(16, 4)  
	declare @suma_brutto_wal decimal(16, 4)  

	exec RM_SumujZamowienie_Server @id_zamowienia, 
		@suma_netto output, @suma_brutto output, @suma_netto_wal output, @suma_brutto_wal output

	update ZAMOWIENIE set
		WARTOSC_BRUTTO = @suma_brutto,
		WARTOSC_NETTO = @suma_netto,
		WARTOSC_BRUTTO_WAL = @suma_brutto_wal,
		WARTOSC_NETTO_WAL = @suma_netto_wal
	where ID_ZAMOWIENIA = @id_zamowienia


	declare @format_numeracji varchar(50)
	declare @okresnumeracji tinyint
	declare @parametr1 tinyint
	declare @parametr2 tinyint

	declare @id_typu int

	select top 1 @id_typu = id_typu from TYP_DOKUMENTU_MAGAZYNOWEGO where NAZWA = 'Zamówienie od odbiorcy' and ID_FIRMY = @IdFirmy

	exec JL_PobierzFormatNumeracji_Server @IdFirmy, 2, @id_typu, @IdMagazynu, 
		@format_numeracji output, @okresnumeracji output, @parametr1 output, @parametr2 output

	declare @autonumer int

	exec RM_ZatwierdzZamowienie_Server @id_zamowienia, @IdKontrahenta, @id_typu, '<auto>', @format_numeracji,
		@okresnumeracji, @parametr1, @parametr2, @autonumer, @IdFirmy, @IdMagazynu, @Data, @Data, 0, 0, 1, 
		'', 1, null,
		1, 0, '', 0, 1, 0, 0.00, 2, '', '', '', 0, '', 0

	update ZAMOWIENIE set
		NR_ZAMOWIENIA_KLIENTA = isNull(@DOKUMWZ, ''),
		UWAGI = isNull(@NRAWIZO, '') +  + CHAR(13)+CHAR(10) + isNull(@UWAGI, '')
	where ID_ZAMOWIENIA = @id_zamowienia

	select  top 1 @NumerZamowienia = NUMER from ZAMOWIENIE where ID_ZAMOWIENIA = @id_zamowienia
		
	if @@trancount>0 commit transaction 
	goto Koniec 
	
	Error: 
	IF (SELECT CURSOR_STATUS('global','kursor_po_poz')) >= -1
	BEGIN
		CLOSE kursor_po_poz
		DEALLOCATE kursor_po_poz
	END


	if @@trancount>0 rollback tran 
	goto Koniec 

	Koniec: 
		set transaction isolation level READ COMMITTED 
		return
	
end 
go

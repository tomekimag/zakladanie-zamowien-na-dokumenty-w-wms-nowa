﻿namespace zakladanieZamowienNaDokumentyWWMS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView_dokumentyDoWprowadzenia = new System.Windows.Forms.DataGridView();
            this.dateTimePicker_dataOd = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker_dataDo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.button_szukaj = new System.Windows.Forms.Button();
            this.button_wszystko = new System.Windows.Forms.Button();
            this.button_wprowadzZamDoDostawcy = new System.Windows.Forms.Button();
            this.button_wprowadzZamOdOdbiorcy = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_dokumentyDoWprowadzenia)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_wprowadzZamOdOdbiorcy);
            this.panel1.Controls.Add(this.button_wprowadzZamDoDostawcy);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 444);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1048, 75);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_wszystko);
            this.panel2.Controls.Add(this.button_szukaj);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.dateTimePicker_dataDo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.dateTimePicker_dataOd);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1048, 67);
            this.panel2.TabIndex = 1;
            // 
            // dataGridView_dokumentyDoWprowadzenia
            // 
            this.dataGridView_dokumentyDoWprowadzenia.AllowUserToAddRows = false;
            this.dataGridView_dokumentyDoWprowadzenia.AllowUserToDeleteRows = false;
            this.dataGridView_dokumentyDoWprowadzenia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_dokumentyDoWprowadzenia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_dokumentyDoWprowadzenia.Location = new System.Drawing.Point(0, 67);
            this.dataGridView_dokumentyDoWprowadzenia.Name = "dataGridView_dokumentyDoWprowadzenia";
            this.dataGridView_dokumentyDoWprowadzenia.ReadOnly = true;
            this.dataGridView_dokumentyDoWprowadzenia.Size = new System.Drawing.Size(1048, 377);
            this.dataGridView_dokumentyDoWprowadzenia.TabIndex = 2;
            this.dataGridView_dokumentyDoWprowadzenia.DataSourceChanged += new System.EventHandler(this.dataGridView_dokumentyDoWprowadzenia_DataSourceChanged);
            this.dataGridView_dokumentyDoWprowadzenia.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_dokumentyDoWprowadzenia_ColumnAdded);
            // 
            // dateTimePicker_dataOd
            // 
            this.dateTimePicker_dataOd.Location = new System.Drawing.Point(72, 10);
            this.dateTimePicker_dataOd.Name = "dateTimePicker_dataOd";
            this.dateTimePicker_dataOd.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker_dataOd.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Data od:";
            // 
            // dateTimePicker_dataDo
            // 
            this.dateTimePicker_dataDo.Location = new System.Drawing.Point(72, 36);
            this.dateTimePicker_dataDo.Name = "dateTimePicker_dataDo";
            this.dateTimePicker_dataDo.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker_dataDo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Data do:";
            // 
            // button_szukaj
            // 
            this.button_szukaj.Location = new System.Drawing.Point(278, 10);
            this.button_szukaj.Name = "button_szukaj";
            this.button_szukaj.Size = new System.Drawing.Size(93, 45);
            this.button_szukaj.TabIndex = 4;
            this.button_szukaj.Text = "Szukaj";
            this.button_szukaj.UseVisualStyleBackColor = true;
            this.button_szukaj.Click += new System.EventHandler(this.button_szukaj_Click);
            // 
            // button_wszystko
            // 
            this.button_wszystko.Location = new System.Drawing.Point(377, 10);
            this.button_wszystko.Name = "button_wszystko";
            this.button_wszystko.Size = new System.Drawing.Size(105, 45);
            this.button_wszystko.TabIndex = 5;
            this.button_wszystko.Text = "Wszystko";
            this.button_wszystko.UseVisualStyleBackColor = true;
            this.button_wszystko.Click += new System.EventHandler(this.button_wszystko_Click);
            // 
            // button_wprowadzZamDoDostawcy
            // 
            this.button_wprowadzZamDoDostawcy.Location = new System.Drawing.Point(21, 26);
            this.button_wprowadzZamDoDostawcy.Name = "button_wprowadzZamDoDostawcy";
            this.button_wprowadzZamDoDostawcy.Size = new System.Drawing.Size(221, 23);
            this.button_wprowadzZamDoDostawcy.TabIndex = 0;
            this.button_wprowadzZamDoDostawcy.Text = "Na zamówienie do dostawcy";
            this.button_wprowadzZamDoDostawcy.UseVisualStyleBackColor = true;
            this.button_wprowadzZamDoDostawcy.Click += new System.EventHandler(this.button_wprowadzZamDoDostawcy_Click);
            // 
            // button_wprowadzZamOdOdbiorcy
            // 
            this.button_wprowadzZamOdOdbiorcy.Location = new System.Drawing.Point(248, 26);
            this.button_wprowadzZamOdOdbiorcy.Name = "button_wprowadzZamOdOdbiorcy";
            this.button_wprowadzZamOdOdbiorcy.Size = new System.Drawing.Size(221, 23);
            this.button_wprowadzZamOdOdbiorcy.TabIndex = 1;
            this.button_wprowadzZamOdOdbiorcy.Text = "Na zamówienie od odbiorcy";
            this.button_wprowadzZamOdOdbiorcy.UseVisualStyleBackColor = true;
            this.button_wprowadzZamOdOdbiorcy.Click += new System.EventHandler(this.button_wprowadzZamOdOdbiorcy_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1048, 519);
            this.Controls.Add(this.dataGridView_dokumentyDoWprowadzenia);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_dokumentyDoWprowadzenia)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView_dokumentyDoWprowadzenia;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker_dataDo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker_dataOd;
        private System.Windows.Forms.Button button_wszystko;
        private System.Windows.Forms.Button button_szukaj;
        private System.Windows.Forms.Button button_wprowadzZamDoDostawcy;
        private System.Windows.Forms.Button button_wprowadzZamOdOdbiorcy;
    }
}


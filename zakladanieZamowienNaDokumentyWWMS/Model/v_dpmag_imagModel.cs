﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zakladanieZamowienNaDokumentyWWMS.Model
{
    public class v_dpmag_imagModel
    {
        public string TYPDOK { get; set; }
        public long REFNO { get; set; }
        public string NRDOKUMENTU { get; set; }
        public Nullable<System.DateTime> DDOWOD { get; set; }
        public Nullable<System.DateTime> DATFAKTURY { get; set; }
        public Nullable<long> NRIDODN { get; set; }
        public long NRIDASN { get; set; }
        public long NRIDWMS { get; set; }
        public string UWAGI { get; set; }
        public string UWAGIPOZ { get; set; }
        public string NRAWIZO { get; set; }
        public string DOKUMWZ { get; set; }
        public decimal ILOSC { get; set; }
    }
}

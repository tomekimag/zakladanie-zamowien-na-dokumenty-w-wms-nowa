﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using zakladanieZamowienNaDokumentyWWMS.Model;
using zakladanieZamowienNaDokumentyWWMS.Controllers;
using System.Data.Entity.Core.Objects;

namespace zakladanieZamowienNaDokumentyWWMS
{
    public partial class Form1 : Form
    {
        private List<v_dpmag_imagModel> dokumenty;

        public Form1()
        {
            InitializeComponent();
            PobierzDaneDoSiatki();
        }

        private void PobierzDaneDoSiatki()
        {
            dataGridView_dokumentyDoWprowadzenia.DataSource = dokumenty = v_dpmag_imagController.PobierzDokumenty();
        }

        private void dataGridView_dokumentyDoWprowadzenia_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "TYPDOK":
                    e.Column.Visible = true;
                    break;
                case "NRDOKUMENTU":
                    e.Column.Visible = true;
                    break;
                case "DDOWOD":
                    e.Column.Visible = true;
                    break;
                case "DATFAKTURY":
                    e.Column.Visible = true;
                    break;
                /*case "NRIDODN":
                    e.Column.Visible = true;
                    break;*/
                default:
                    e.Column.Visible = false;
                    break;
            }
        }

        private void dataGridView_dokumentyDoWprowadzenia_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void button_szukaj_Click(object sender, EventArgs e)
        {
            //MessageBox.Show($@"{dateTimePicker_dataOd.Value} {dateTimePicker_dataDo.Value}");

            DateTime dataOd = dateTimePicker_dataOd.Value;
            DateTime dataDo = dateTimePicker_dataDo.Value;

            dataOd = dataOd.AddHours(dataOd.Hour*(-1));
            dataOd = dataOd.AddMinutes(dataOd.Minute * (-1));
            dataOd = dataOd.AddSeconds(dataOd.Second * (-1));
            dataOd = dataOd.AddMilliseconds(dataOd.Millisecond * (-1));

            dataDo = dataDo.AddHours(dataDo.Hour * (-1));
            dataDo = dataDo.AddMinutes(dataDo.Minute * (-1));
            dataDo = dataDo.AddSeconds(dataDo.Second * (-1));
            dataDo = dataDo.AddMilliseconds(dataDo.Millisecond * (-1));
            dataDo = dataDo.AddDays(1);
            dataDo = dataDo.AddMilliseconds(-1);

            MessageBox.Show($@"{dataOd} {dataDo}");

            List<v_dpmag_imagModel> dokumentyWybrane = dokumenty.Where(x => x.DATFAKTURY >= dataOd && x.DATFAKTURY <= dataDo).ToList();
            dataGridView_dokumentyDoWprowadzenia.DataSource = dokumentyWybrane;
        }

        private void button_wszystko_Click(object sender, EventArgs e)
        {
            dataGridView_dokumentyDoWprowadzenia.DataSource = dokumenty;
        }

        private void button_wprowadzZamDoDostawcy_Click(object sender, EventArgs e)
        {
            if (dataGridView_dokumentyDoWprowadzenia.SelectedRows == null)
            {
                MessageBox.Show("Wskarz dokument.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_dokumentyDoWprowadzenia.SelectedRows.Count != 0)
            {
                long refno = long.Parse(dataGridView_dokumentyDoWprowadzenia["REFNO", dataGridView_dokumentyDoWprowadzenia.SelectedRows[0].Index].Value.ToString());

                string numerZamowienia = string.Empty;

                try
                {
                    ObjectParameter _numerZamowienia = new ObjectParameter("numerZamowienia", typeof(string));
                    using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
                    {
                        //MessageBox.Show($@"twit refno={refno};idFirmy={ParametryUruchomienioweController.idFirmy}; idMagazynu={ParametryUruchomienioweController.idMagazynu}; idUzytkownika = {ParametryUruchomienioweController.idUzytkownika}");

                        entity.IMAG_twit_dodaj_zamowienia_do_dostawcy_z_WMS(ParametryUruchomienioweController.idFirmy, ParametryUruchomienioweController.idMagazynu,
                            ParametryUruchomienioweController.idUzytkownika, refno, _numerZamowienia);
                        numerZamowienia = _numerZamowienia.Value.ToString();
                    }
                    if (numerZamowienia != string.Empty)
                    {
                        MessageBox.Show($@"Zostało utworzone zamówienie o numerze {numerZamowienia}.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show($@"Zamówienie nie zostało utworzone.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show($@"Błąd tworzenia zamówienia : {ex.Message} {ex.InnerException.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void button_wprowadzZamOdOdbiorcy_Click(object sender, EventArgs e)
        {
            if (dataGridView_dokumentyDoWprowadzenia.SelectedRows == null)
            {
                MessageBox.Show("Wskarz dokument.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_dokumentyDoWprowadzenia.SelectedRows.Count != 0)
            {
                long refno = long.Parse(dataGridView_dokumentyDoWprowadzenia["REFNO", dataGridView_dokumentyDoWprowadzenia.SelectedRows[0].Index].Value.ToString());

                string numerZamowienia = string.Empty;

                try
                {
                    ObjectParameter _numerZamowienia = new ObjectParameter("numerZamowienia", typeof(string));
                    using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
                    {
                        //MessageBox.Show($@"twit refno={refno};idFirmy={ParametryUruchomienioweController.idFirmy}; idMagazynu={ParametryUruchomienioweController.idMagazynu}; idUzytkownika = {ParametryUruchomienioweController.idUzytkownika}");

                        entity.IMAG_twit_dodaj_zamowienia_od_odbiorcy_z_WMS(ParametryUruchomienioweController.idFirmy, ParametryUruchomienioweController.idMagazynu,
                            ParametryUruchomienioweController.idUzytkownika, refno, _numerZamowienia);
                        numerZamowienia = _numerZamowienia.Value.ToString();
                    }
                    if (numerZamowienia != string.Empty)
                    {
                        MessageBox.Show($@"Zostało utworzone zamówienie o numerze {numerZamowienia}.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show($@"Zamówienie nie zostało utworzone.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($@"Błąd tworzenia zamówienia : {ex.Message} {ex.InnerException.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}

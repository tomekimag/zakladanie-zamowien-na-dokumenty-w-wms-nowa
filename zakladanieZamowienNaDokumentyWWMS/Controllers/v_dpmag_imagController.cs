﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zakladanieZamowienNaDokumentyWWMS.Model;

namespace zakladanieZamowienNaDokumentyWWMS.Controllers
{
    public static class v_dpmag_imagController
    {
        public static List<v_dpmag_imagModel> PobierzDokumenty()
        {
            List<v_dpmag_imagModel> dokumenty;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                dokumenty = (from z in entity.v_dpmag_imag
                             select new v_dpmag_imagModel()
                             {
                                 TYPDOK = z.TYPDOK,
                                 REFNO = z.REFNO,
                                 NRDOKUMENTU = z.NRDOKUMENTU,
                                 DDOWOD = z.DDOWOD,
                                 DATFAKTURY = z.DATFAKTURY,
                                 NRIDODN = z.NRIDODN
                             }).Distinct().OrderBy(x=> x.NRDOKUMENTU).ToList();
            }

            return dokumenty;
        }
    }
}
